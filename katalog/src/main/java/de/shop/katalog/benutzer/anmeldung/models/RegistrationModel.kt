package de.shop.katalog.benutzer.anmeldung.models

import de.shop.katalog.global.annotationen.FeldMatcher
import javax.validation.constraints.NotBlank

@FeldMatcher.List(
        FeldMatcher(feld = "passwort", passendesFeld = "passwortWiederholung", message = "Passwörter stimmen nicht überein." ),
        FeldMatcher(feld = "email", passendesFeld = "emailWiederholung", message = "E-Mails stimmen nicht überein.")
)
class RegistrationModel(
        @get:NotBlank val benutzername: String,
        @get:NotBlank() val email: String,
        @get:NotBlank() val emailWiederholung: String,
        @get:NotBlank() val passwort: String,
        @get:NotBlank() val passwortWiederholung: String
)