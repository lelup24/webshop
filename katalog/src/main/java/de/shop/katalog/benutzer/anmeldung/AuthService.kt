package de.shop.katalog.benutzer.anmeldung

import de.shop.katalog.benutzer.anmeldung.jwt.JwtAuthTokenProvider
import de.shop.katalog.benutzer.anmeldung.models.RegistrationModel
import de.shop.katalog.benutzer.anmeldung.models.TokenResponseModel
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class AuthService(
        private val tokenProvider: JwtAuthTokenProvider,
        private val auth: DaoAuthenticationProvider
) {

    fun anmelden(model: AnmeldungModel): TokenResponseModel {

        val authentication: Authentication =
                  auth.authenticate(UsernamePasswordAuthenticationToken(model.email, model.passwort))

        SecurityContextHolder.getContext().setAuthentication(authentication)

        val token: String = tokenProvider.generateJwtToken(authentication)

        return TokenResponseModel(token)

    }

    fun registrieren(model: RegistrationModel) {

    }

    private fun passwortPruefen() {

    }

    private fun benutzerExists(email: String) {

    }

}