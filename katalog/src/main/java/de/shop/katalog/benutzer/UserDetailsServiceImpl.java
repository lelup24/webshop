package de.shop.katalog.benutzer;

import de.shop.katalog.data.jooq.tables.daos.BenutzerDao;
import de.shop.katalog.data.jooq.tables.pojos.Benutzer;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static de.shop.katalog.data.jooq.tables.Benutzer.BENUTZER;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final BenutzerDao benutzerDao;

    public UserDetailsServiceImpl(BenutzerDao benutzerDao) {
        this.benutzerDao = benutzerDao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        final Benutzer benutzer = benutzerDao.fetchOne(BENUTZER.EMAIL, email);
        if (benutzer == null) {
            throw  new UsernameNotFoundException("Nutzer nicht gefunden");
        }

        return new User(benutzer.getEmail(), benutzer.getPasswort(), Collections.singleton(new SimpleGrantedAuthority("ADMIN")));
    }
}
