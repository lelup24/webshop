package de.shop.katalog.benutzer.kafka.consumer;

import de.shop.katalog.configs.KafkaConsumerConfig;
import de.shop.katalog.data.jooq.tables.pojos.Benutzer;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

@Service
public class RegistrationListener {

    @KafkaListener(topics="anmeldung", groupId = "default", containerFactory = "kafkaListenerContainerFactory3")
    @SendTo
    public String reply(Benutzer request) {
        return "REPLY " + request.getBenutzername();
    }
}
