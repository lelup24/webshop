package de.shop.katalog.benutzer.kafka.producer;

import de.shop.katalog.data.jooq.tables.pojos.Benutzer;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class RegistrationSender {


    private final ReplyingKafkaTemplate<String, Benutzer, String> replyingKafkaTemplate;

    public RegistrationSender(ReplyingKafkaTemplate<String, Benutzer, String> replyingKafkaTemplate) {
        this.replyingKafkaTemplate = replyingKafkaTemplate;
    }


    public void sendeAnmeldung(final Benutzer benutzer) {
        ProducerRecord<String, Benutzer> producerRecord = new ProducerRecord<>("anmeldung", benutzer);

        producerRecord.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, "token".getBytes()));

        RequestReplyFuture<String, Benutzer, String> sendAndReceive = replyingKafkaTemplate.sendAndReceive(producerRecord);

        sendAndReceive.addCallback(
            new ListenableFutureCallback<ConsumerRecord<String, String>>() {
              @Override
              public void onFailure(Throwable ex) {
                System.out.println(ex);
              }

              @Override
              public void onSuccess(ConsumerRecord<String, String> result) {
                  SendResult<String, Benutzer> sendResult = null;
                  try {
                      sendResult = sendAndReceive.getSendFuture().get();
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  } catch (ExecutionException e) {
                      e.printStackTrace();
                  }

                  sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));

                  ConsumerRecord<String, String> consumerRecord = null;
                  try {
                      consumerRecord = sendAndReceive.get();
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  } catch (ExecutionException e) {
                      e.printStackTrace();
                  }
                  System.out.println(consumerRecord.value());
              }

            });

    }
}
