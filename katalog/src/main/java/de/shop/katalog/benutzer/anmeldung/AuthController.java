package de.shop.katalog.benutzer.anmeldung;

import de.shop.katalog.benutzer.anmeldung.models.RegistrationModel;
import de.shop.katalog.benutzer.anmeldung.models.TokenResponseModel;
import de.shop.katalog.global.annotationen.ApiRestController;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@ApiRestController
public class AuthController {

  private AuthService anmeldungService;


  public AuthController(AuthService anmeldungService) {
    this.anmeldungService = anmeldungService;
  }

  @PostMapping("/registrieren")
  public ResponseEntity<Void> registrieren(@Valid @RequestBody RegistrationModel registrationModel) {
    System.out.println(registrationModel.getBenutzername());
    return ResponseEntity.noContent().build();
  }

  @PostMapping("/anmelden")
  public ResponseEntity<TokenResponseModel> anmelden(@RequestBody AnmeldungModel anmeldungModel) {
    return new ResponseEntity<>(anmeldungService.anmelden(anmeldungModel), HttpStatus.OK);
  }
}
