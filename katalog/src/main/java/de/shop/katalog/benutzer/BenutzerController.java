package de.shop.katalog.benutzer;

import de.shop.katalog.benutzer.kafka.producer.RegistrationSender;
import de.shop.katalog.data.jooq.tables.daos.BenutzerDao;
import de.shop.katalog.data.jooq.tables.pojos.Benutzer;

import java.lang.reflect.Field;
import java.util.concurrent.ExecutionException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class BenutzerController {

    private final BenutzerDao benutzerDao;
    private final RegistrationSender registrationSender;


    public BenutzerController(BenutzerDao benutzerDao, RegistrationSender registrationSender) {
        this.benutzerDao = benutzerDao;
        this.registrationSender = registrationSender;
    }

    @GetMapping("/")
    public ResponseEntity<Void> add() {
           Benutzer benutzer =  new Benutzer();
           benutzer.setBenutzername("Karlö");
           benutzer.setEmail("peter.q");
           benutzer.setId(UUID.randomUUID().toString().replace("-", ""));
           benutzer.setVorname("p");
           benutzer.setNachname("k");

        registrationSender.sendeAnmeldung(benutzer);

        Field[] fields = benutzer.getClass().getDeclaredFields();
        System.out.println(benutzer.getClass().getName());

        for (Field field: fields
             ) {
            System.out.println(field.getName());
        }

      return ResponseEntity.noContent().build();
    }
}
