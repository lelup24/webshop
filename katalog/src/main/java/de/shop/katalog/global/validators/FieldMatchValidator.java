package de.shop.katalog.global.validators;

import de.shop.katalog.global.annotationen.FeldMatcher;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapperImpl;

public class FieldMatchValidator implements ConstraintValidator<FeldMatcher, Object> {

  private String erstesFeld;
  private String zweitesFeld;
  private String nachricht;

  @Override
  public void initialize(final FeldMatcher constraintAnnotation) {
    erstesFeld = constraintAnnotation.feld();
    zweitesFeld = constraintAnnotation.passendesFeld();
    nachricht = constraintAnnotation.message();
  }

  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    boolean valid = true;
    try
    {
      final Object firstObj = new BeanWrapperImpl(value).getPropertyValue(erstesFeld);
      final Object secondObj = new BeanWrapperImpl(value).getPropertyValue(zweitesFeld);

      valid =  firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);
    }
    catch (final Exception ignore)
    {
      // ignore
    }

    if (!valid){
      context.buildConstraintViolationWithTemplate(nachricht)
          .addPropertyNode(erstesFeld)
          .addConstraintViolation()
          .disableDefaultConstraintViolation();
    }

    return valid;
  }
}
