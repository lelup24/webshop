package de.shop.katalog.global.pagination

import org.jooq.SortOrder

interface IPagination {
    var limit: Int
    var offset: Int
    var sortOrder: SortOrder

}