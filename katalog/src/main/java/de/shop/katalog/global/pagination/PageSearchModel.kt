package de.shop.katalog.global.pagination

import org.jooq.SortOrder

open class PageSearchModel(
        override var limit: Int,
        override var offset: Int,
        override var sortOrder: SortOrder) : IPagination