package de.shop.katalog.global.pagination

class PaginationResultModel<E>(val result: List<E>, val totalCount: Int, val sliceCount: Int)