package de.shop.katalog.produkte.empfehlungen

import de.shop.katalog.global.annotationen.ApiRestController
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping

@ApiRestController
class EmpfehlungenController {

    @GetMapping("/produkte/empfehlungen")
    fun getEmpfehlungen(): ResponseEntity<ProduktEmpfehlungenModel> {
        return ResponseEntity(null, HttpStatus.OK)
    }
}