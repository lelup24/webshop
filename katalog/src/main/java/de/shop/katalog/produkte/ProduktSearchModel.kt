package de.shop.katalog.produkte

import de.shop.katalog.global.pagination.PageSearchModel
import org.jooq.SortOrder

data class ProduktSearchModel(override var limit: Int, override var offset: Int, override var sortOrder: SortOrder) : PageSearchModel(limit, offset, sortOrder) {
    var name: String? = null

}