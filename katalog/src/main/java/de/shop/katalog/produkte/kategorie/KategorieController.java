package de.shop.katalog.produkte.kategorie;

import de.shop.katalog.global.annotationen.ApiRestController;
import de.shop.katalog.produkte.ProduktResultModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@ApiRestController
public class KategorieController {

    private final KategorieService kategorieService;

    public KategorieController(KategorieService kategorieService) {
        this.kategorieService = kategorieService;
    }

    @GetMapping("/produkte/kategorie")
    public ResponseEntity<List<ProduktResultModel>> getKategorieWithSuchtwort
            (KategorieSucheModel sucheModel) {
        return new ResponseEntity<>(kategorieService.getProdukteByKategorieAndSuchwort(sucheModel),
                HttpStatus.ACCEPTED);
    }
}
