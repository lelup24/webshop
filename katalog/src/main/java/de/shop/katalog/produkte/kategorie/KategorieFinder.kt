package de.shop.katalog.produkte.kategorie

import de.shop.katalog.data.jooq.tables.Produkt.PRODUKT
import de.shop.katalog.data.jooq.tables.ProduktKategorie.PRODUKT_KATEGORIE
import de.shop.katalog.produkte.ProduktResultModel
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.SelectFieldOrAsterisk
import org.jooq.SelectOnConditionStep
import org.jooq.impl.DSL
import org.springframework.stereotype.Component

@Component
class KategorieFinder(val dsl: DSLContext) {

    fun getProdukteByKategorieAndSuchwort(suchModel: KategorieSucheModel) :
            List<ProduktResultModel>?   {
        println(suchModel.kategorie)
        return select(DSL.asterisk())
                .where(PRODUKT_KATEGORIE.KATEGORIE_ID.eq(suchModel.kategorie))
                .orderBy(PRODUKT.BEZEICHNUNG.sort(suchModel.sortOrder))
                .limit(suchModel.limit)
                .offset(suchModel.offset)
                ?.map { record ->
                    ProduktResultModel(record.get(PRODUKT.BEZEICHNUNG)) }
    }

    private fun select (fields: SelectFieldOrAsterisk): SelectOnConditionStep<Record> {
        return dsl
                .select(fields)
                .from(PRODUKT)
                .join(PRODUKT_KATEGORIE)
                .on(PRODUKT.ID.eq(PRODUKT_KATEGORIE.PRODUKT_ID))
    }
}