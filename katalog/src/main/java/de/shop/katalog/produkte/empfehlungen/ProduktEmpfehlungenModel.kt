package de.shop.katalog.produkte.empfehlungen

data class ProduktEmpfehlungenModel(
        val id: String,
        val bezeichnung: String,
        val beschreibung: String
)