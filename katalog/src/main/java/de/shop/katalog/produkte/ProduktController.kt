package de.shop.katalog.produkte

import de.shop.katalog.global.annotationen.ApiRestController
import de.shop.katalog.global.pagination.PaginationResultModel
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import java.util.*

@ApiRestController
class ProduktController {

    @GetMapping("/produkte/{id}")
    fun getProdukt(@PathVariable id: UUID): ResponseEntity<ProduktModel> {
        return ResponseEntity<ProduktModel>(ProduktModel(), HttpStatus.ACCEPTED)
    }

    @GetMapping("/produkte")
    fun getProdukte(suchModel: ProduktSearchModel): ResponseEntity<PaginationResultModel<ProduktResultModel>> {
        println("" + suchModel.limit + "" +suchModel.sortOrder)
        if (suchModel.name != null) println(suchModel.name)
        return ResponseEntity(PaginationResultModel(Collections.singletonList(ProduktResultModel("Peter")), 0, 0), HttpStatus.OK)
    }


}