package de.shop.katalog.produkte.kategorie

import de.shop.katalog.global.pagination.PageSearchModel
import org.jooq.SortOrder

data class KategorieSucheModel(
        var kategorie: String?,
        var suchwort: String?,
        override var limit: Int,
        override var offset: Int,
        override var sortOrder: SortOrder) : PageSearchModel(limit, offset, sortOrder)