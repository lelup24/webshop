package de.shop.katalog.produkte.kategorie

import de.shop.katalog.produkte.ProduktResultModel
import org.springframework.stereotype.Service


@Service
class KategorieService(val kategorieFinder: KategorieFinder) {


    fun getProdukteByKategorieAndSuchwort(suchModel: KategorieSucheModel) :
            List<ProduktResultModel>? {
        return kategorieFinder.getProdukteByKategorieAndSuchwort(suchModel)
    }



}