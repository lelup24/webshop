/*
 * This file is generated by jOOQ.
 */
package de.shop.katalog.data.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Produkt implements Serializable {

    private static final long serialVersionUID = 1855431916;

    private String  id;
    private String  herstellerId;
    private String  bezeichnung;
    private String  beschreibung;
    private Double  preis;
    private Integer preisnachlass;
    private Byte    angebot;

    public Produkt() {}

    public Produkt(Produkt value) {
        this.id = value.id;
        this.herstellerId = value.herstellerId;
        this.bezeichnung = value.bezeichnung;
        this.beschreibung = value.beschreibung;
        this.preis = value.preis;
        this.preisnachlass = value.preisnachlass;
        this.angebot = value.angebot;
    }

    public Produkt(
        String  id,
        String  herstellerId,
        String  bezeichnung,
        String  beschreibung,
        Double  preis,
        Integer preisnachlass,
        Byte    angebot
    ) {
        this.id = id;
        this.herstellerId = herstellerId;
        this.bezeichnung = bezeichnung;
        this.beschreibung = beschreibung;
        this.preis = preis;
        this.preisnachlass = preisnachlass;
        this.angebot = angebot;
    }

    @NotNull
    @Size(max = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NotNull
    @Size(max = 32)
    public String getHerstellerId() {
        return this.herstellerId;
    }

    public void setHerstellerId(String herstellerId) {
        this.herstellerId = herstellerId;
    }

    @NotNull
    @Size(max = 50)
    public String getBezeichnung() {
        return this.bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    @NotNull
    @Size(max = 16777215)
    public String getBeschreibung() {
        return this.beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    @NotNull
    public Double getPreis() {
        return this.preis;
    }

    public void setPreis(Double preis) {
        this.preis = preis;
    }

    public Integer getPreisnachlass() {
        return this.preisnachlass;
    }

    public void setPreisnachlass(Integer preisnachlass) {
        this.preisnachlass = preisnachlass;
    }

    public Byte getAngebot() {
        return this.angebot;
    }

    public void setAngebot(Byte angebot) {
        this.angebot = angebot;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Produkt (");

        sb.append(id);
        sb.append(", ").append(herstellerId);
        sb.append(", ").append(bezeichnung);
        sb.append(", ").append(beschreibung);
        sb.append(", ").append(preis);
        sb.append(", ").append(preisnachlass);
        sb.append(", ").append(angebot);

        sb.append(")");
        return sb.toString();
    }
}
