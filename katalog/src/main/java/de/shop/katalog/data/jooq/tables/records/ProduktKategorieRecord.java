/*
 * This file is generated by jOOQ.
 */
package de.shop.katalog.data.jooq.tables.records;


import de.shop.katalog.data.jooq.tables.ProduktKategorie;

import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProduktKategorieRecord extends UpdatableRecordImpl<ProduktKategorieRecord> implements Record3<String, String, String> {

    private static final long serialVersionUID = 473961149;

    /**
     * Setter for <code>katalog.produkt_kategorie.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>katalog.produkt_kategorie.id</code>.
     */
    @NotNull
    @Size(max = 32)
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>katalog.produkt_kategorie.produkt_id</code>.
     */
    public void setProduktId(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>katalog.produkt_kategorie.produkt_id</code>.
     */
    @NotNull
    @Size(max = 32)
    public String getProduktId() {
        return (String) get(1);
    }

    /**
     * Setter for <code>katalog.produkt_kategorie.kategorie_id</code>.
     */
    public void setKategorieId(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>katalog.produkt_kategorie.kategorie_id</code>.
     */
    @NotNull
    @Size(max = 32)
    public String getKategorieId() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<String, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<String, String, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return ProduktKategorie.PRODUKT_KATEGORIE.ID;
    }

    @Override
    public Field<String> field2() {
        return ProduktKategorie.PRODUKT_KATEGORIE.PRODUKT_ID;
    }

    @Override
    public Field<String> field3() {
        return ProduktKategorie.PRODUKT_KATEGORIE.KATEGORIE_ID;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getProduktId();
    }

    @Override
    public String component3() {
        return getKategorieId();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getProduktId();
    }

    @Override
    public String value3() {
        return getKategorieId();
    }

    @Override
    public ProduktKategorieRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public ProduktKategorieRecord value2(String value) {
        setProduktId(value);
        return this;
    }

    @Override
    public ProduktKategorieRecord value3(String value) {
        setKategorieId(value);
        return this;
    }

    @Override
    public ProduktKategorieRecord values(String value1, String value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProduktKategorieRecord
     */
    public ProduktKategorieRecord() {
        super(ProduktKategorie.PRODUKT_KATEGORIE);
    }

    /**
     * Create a detached, initialised ProduktKategorieRecord
     */
    public ProduktKategorieRecord(String id, String produktId, String kategorieId) {
        super(ProduktKategorie.PRODUKT_KATEGORIE);

        set(0, id);
        set(1, produktId);
        set(2, kategorieId);
    }
}
