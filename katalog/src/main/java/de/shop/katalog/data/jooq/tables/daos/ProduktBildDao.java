/*
 * This file is generated by jOOQ.
 */
package de.shop.katalog.data.jooq.tables.daos;


import de.shop.katalog.data.jooq.tables.ProduktBild;
import de.shop.katalog.data.jooq.tables.records.ProduktBildRecord;

import java.util.List;

import javax.annotation.processing.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Repository
public class ProduktBildDao extends DAOImpl<ProduktBildRecord, de.shop.katalog.data.jooq.tables.pojos.ProduktBild, String> {

    /**
     * Create a new ProduktBildDao without any configuration
     */
    public ProduktBildDao() {
        super(ProduktBild.PRODUKT_BILD, de.shop.katalog.data.jooq.tables.pojos.ProduktBild.class);
    }

    /**
     * Create a new ProduktBildDao with an attached configuration
     */
    @Autowired
    public ProduktBildDao(Configuration configuration) {
        super(ProduktBild.PRODUKT_BILD, de.shop.katalog.data.jooq.tables.pojos.ProduktBild.class, configuration);
    }

    @Override
    public String getId(de.shop.katalog.data.jooq.tables.pojos.ProduktBild object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<de.shop.katalog.data.jooq.tables.pojos.ProduktBild> fetchRangeOfId(String lowerInclusive, String upperInclusive) {
        return fetchRange(ProduktBild.PRODUKT_BILD.ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<de.shop.katalog.data.jooq.tables.pojos.ProduktBild> fetchById(String... values) {
        return fetch(ProduktBild.PRODUKT_BILD.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public de.shop.katalog.data.jooq.tables.pojos.ProduktBild fetchOneById(String value) {
        return fetchOne(ProduktBild.PRODUKT_BILD.ID, value);
    }

    /**
     * Fetch records that have <code>produkt_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<de.shop.katalog.data.jooq.tables.pojos.ProduktBild> fetchRangeOfProduktId(String lowerInclusive, String upperInclusive) {
        return fetchRange(ProduktBild.PRODUKT_BILD.PRODUKT_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>produkt_id IN (values)</code>
     */
    public List<de.shop.katalog.data.jooq.tables.pojos.ProduktBild> fetchByProduktId(String... values) {
        return fetch(ProduktBild.PRODUKT_BILD.PRODUKT_ID, values);
    }

    /**
     * Fetch records that have <code>bild_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<de.shop.katalog.data.jooq.tables.pojos.ProduktBild> fetchRangeOfBildId(String lowerInclusive, String upperInclusive) {
        return fetchRange(ProduktBild.PRODUKT_BILD.BILD_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>bild_id IN (values)</code>
     */
    public List<de.shop.katalog.data.jooq.tables.pojos.ProduktBild> fetchByBildId(String... values) {
        return fetch(ProduktBild.PRODUKT_BILD.BILD_ID, values);
    }
}
