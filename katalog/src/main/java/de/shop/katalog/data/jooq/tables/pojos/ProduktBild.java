/*
 * This file is generated by jOOQ.
 */
package de.shop.katalog.data.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProduktBild implements Serializable {

    private static final long serialVersionUID = -1620646080;

    private String id;
    private String produktId;
    private String bildId;

    public ProduktBild() {}

    public ProduktBild(ProduktBild value) {
        this.id = value.id;
        this.produktId = value.produktId;
        this.bildId = value.bildId;
    }

    public ProduktBild(
        String id,
        String produktId,
        String bildId
    ) {
        this.id = id;
        this.produktId = produktId;
        this.bildId = bildId;
    }

    @NotNull
    @Size(max = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NotNull
    @Size(max = 32)
    public String getProduktId() {
        return this.produktId;
    }

    public void setProduktId(String produktId) {
        this.produktId = produktId;
    }

    @NotNull
    @Size(max = 32)
    public String getBildId() {
        return this.bildId;
    }

    public void setBildId(String bildId) {
        this.bildId = bildId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ProduktBild (");

        sb.append(id);
        sb.append(", ").append(produktId);
        sb.append(", ").append(bildId);

        sb.append(")");
        return sb.toString();
    }
}
