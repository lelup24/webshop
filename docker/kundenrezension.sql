CREATE DATABASE IF NOT EXISTS `kundenrezension`;

USE kundenrezension;

CREATE TABLE IF NOT EXISTS `benutzer` (
  id varchar(36) not null,
  benutzername varchar(100) not null,
  bild_url varchar(100) not null ,

  primary key (id)
);

CREATE TABLE IF NOT EXISTS `produkt` (
    id varchar(36) not null,
    produkt_link varchar(100) not null,

    primary key (id)
);

CREATE TABLE IF NOT EXISTS `kundenrezension` (
    id varchar(36) not null,
    benutzer_id varchar(36) not null,
    produkt_id varchar(36) not null,
    wert int(5) not null,
    begruendung mediumtext not null,
    erstellt_am bigint not null,
    nuetzlich integer,

    primary key (id),
    foreign key (benutzer_id) references benutzer (id),
    foreign key (produkt_id) references produkt (id)
);

CREATE TABLE IF NOT EXISTS `kommentar` (
   id varchar(36) not null,
   benutzer_id varchar(36) not null,
   produkt_id varchar(36) not null,
   rezension_id varchar(36) not null,
   inhalt mediumtext not null,
   erstellt_am bigint not null,

   primary key (id),
   foreign key (benutzer_id) references benutzer (id),
   foreign key (rezension_id) references kundenrezension (id),
   foreign key  (produkt_id) references  produkt (id)
);