 CREATE DATABASE IF NOT EXISTS `katalog`;
 USE `katalog`;
 
 CREATE TABLE IF NOT EXISTS `benutzer`
 (
	id VARCHAR(36) NOT NULL,
	benutzername VARCHAR(20) NOT NULL,
	email VARCHAR(50) NOT NULL,
	passwort VARCHAR(100) NOT NULL,
	vorname VARCHAR(20) NOT NULL,
	nachname VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)
    );
    
 CREATE TABLE IF NOT EXISTS `hersteller`
 (
	id VARCHAR(36) NOT NULL,
	name VARCHAR(50) NOT NULL,
	beschreibung mediumtext NOT NULL,
	logo_url VARCHAR(100),
    PRIMARY KEY (id)
 );
 
  CREATE TABLE IF NOT EXISTS `produkt`
 (
	id VARCHAR(36) NOT NULL,
	hersteller_id VARCHAR(36) NOT NULL,
	bezeichnung VARCHAR(50) NOT NULL,
	beschreibung mediumtext NOT NULL,
	preis float(4) NOT NULL,
    preisnachlass int,
    angebot tinyint(1),
    angeboten_seit long not null,
    PRIMARY KEY (id),
    FOREIGN KEY (hersteller_id) REFERENCES `hersteller` (id)
 );
 
  CREATE TABLE IF NOT EXISTS `kategorie`
 (
	id VARCHAR(36) NOT NULL,
	bezeichnung VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
 );
 
 CREATE TABLE IF NOT EXISTS `produkt_kategorie`
 (
	id VARCHAR(36) NOT NULL,
	produkt_id VARCHAR(36) NOT NULL,
	kategorie_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (produkt_id) REFERENCES `produkt` (id),
    FOREIGN KEY (kategorie_id) REFERENCES `kategorie` (id)
);

  CREATE TABLE IF NOT EXISTS `bild`
 (
	id VARCHAR(36) NOT NULL,
	bezeichnung VARCHAR(50) NOT NULL,
	url VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

 CREATE TABLE IF NOT EXISTS `produkt_bild`
 (
	id VARCHAR(36) NOT NULL,
	produkt_id VARCHAR(36) NOT NULL,
	bild_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (produkt_id) REFERENCES `produkt` (id),
    FOREIGN KEY (bild_id) REFERENCES `bild` (id)
);