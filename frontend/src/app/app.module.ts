import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartseiteComponent } from './katalog/startseite/startseite.component';
import { ProduktUebersichtItemComponent } from './global/components/produkt-uebersicht-item/produkt-uebersicht-item.component';
import { AngeboteComponent } from './katalog/startseite/components/angebote/angebote.component';
import { NeuheitenComponent } from './katalog/startseite/components/neuheiten/neuheiten.component';
import { EmpfohleneProdukteComponent } from './katalog/startseite/components/empfohlene-produkte/empfohlene-produkte.component';
import { EmpfohleneKategorieComponent } from './katalog/startseite/components/empfohlene-kategorie/empfohlene-kategorie.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NavigationComponent } from './global/components/navigation/navigation.component';
import { MatInputModule } from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import { SuchfeldComponent } from './global/components/suchfeld/suchfeld.component';
import {MatButtonModule} from "@angular/material/button";
import { SelectComponent } from './global/components/select/select.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { FooterComponent } from './global/components/footer/footer.component';
import { KategorieSeiteComponent } from './katalog/kategorie-seite/kategorie-seite.component';
import {HttpClientModule} from "@angular/common/http";
import { ItemUebersichtComponent } from './global/components/item-uebersicht/item-uebersicht.component';

@NgModule({
  declarations: [
    AppComponent,
    StartseiteComponent,
    ProduktUebersichtItemComponent,
    AngeboteComponent,
    NeuheitenComponent,
    EmpfohleneProdukteComponent,
    EmpfohleneKategorieComponent,
    NavigationComponent,
    SuchfeldComponent,
    SelectComponent,
    FooterComponent,
    KategorieSeiteComponent,
    ItemUebersichtComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
