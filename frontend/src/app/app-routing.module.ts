import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StartseiteComponent} from "./katalog/startseite/startseite.component";
import {KategorieSeiteComponent} from "./katalog/kategorie-seite/kategorie-seite.component";


const routes: Routes = [
  { path: '', component: StartseiteComponent },
  { path: 'kategorie', component: KategorieSeiteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
