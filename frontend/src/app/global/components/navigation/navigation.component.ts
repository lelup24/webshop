import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {SuchkomponentModel} from "../../models/suchkomponent.model";
import {KategorieService} from "../../../katalog/kategorie-seite/kategorie.service";

@Component({
  selector: 'shop-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Output() test = new EventEmitter();

  public options: string[] = [
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323','kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',
    'kat1', 'kat2', 'kat3 21323 23323 kat3 21323 23323',

  ];

  constructor(private router: Router, private kategorieService: KategorieService) { }

  ngOnInit(): void {
  }

  public onSuche(model: SuchkomponentModel) {
    this.router.navigate(['kategorie'],
      {queryParams: {kategorie: model.kategorie, suchwort: model.suchwort}});
    this.kategorieService.getKategorieWithSuchwort('0', '10', 'DESC', model.kategorie, model.suchwort);
  }

}
