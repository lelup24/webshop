import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {SuchkomponentModel} from "../../models/suchkomponent.model";

@Component({
  selector: 'shop-suchfeld',
  templateUrl: './suchfeld.component.html',
  styleUrls: ['./suchfeld.component.scss']
})
export class SuchfeldComponent implements OnInit {
  @Output() private suche = new EventEmitter<any>();
  @Input() options: string[];
  @ViewChild('input') inputEl: ElementRef;
  public kategorie: string;
  public suchwort: string;

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
  ) {
    this.iconRegistry.addSvgIcon(
      'suche',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/search-24px.svg'));
  }

  ngOnInit(): void {
  }

  public onAuswahl() {
    this.inputEl.nativeElement.focus();
  }

  public suchen() {
    const anfrage: SuchkomponentModel = {kategorie: this.kategorie, suchwort: this.suchwort};
    this.suche.emit(anfrage)
  }

}
