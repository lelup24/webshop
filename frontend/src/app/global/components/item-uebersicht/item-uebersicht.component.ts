import {Component, Input, OnInit} from '@angular/core';
import {ProduktUebersichtItemModel} from "../../models/produkt-uebersicht-item.model";

@Component({
  selector: 'shop-item-uebersicht',
  templateUrl: './item-uebersicht.component.html',
  styleUrls: ['./item-uebersicht.component.scss']
})
export class ItemUebersichtComponent implements OnInit {
  @Input()
  public produkte: ProduktUebersichtItemModel[];

  constructor() { }

  ngOnInit(): void {
  }

}
