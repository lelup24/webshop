import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemUebersichtComponent } from './item-uebersicht.component';

describe('ItemUebersichtComponent', () => {
  let component: ItemUebersichtComponent;
  let fixture: ComponentFixture<ItemUebersichtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemUebersichtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemUebersichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
