import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef, HostListener,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'shop-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent implements OnInit, ControlValueAccessor{
  @Input() public options;
  @Output() public onUpdate = new EventEmitter();
  @ViewChild('auswahl') auswahl: ElementRef;
  @ViewChild('optionen') optionen: ElementRef;
  @ViewChild('container') container: ElementRef;
  private isOffen = false;
  public ausgewaehlt = 'Kategorie wählen';
  private letzteAktiveOption: HTMLDivElement = null;

  public value = "Peter";
  private onChange: any = () => {};
  private onTouch: any = () => {};

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
  }

  public open() {
    this. isOffen ?
      this.renderer.setStyle(this.optionen.nativeElement, 'display', 'none')
      : this.renderer.setStyle(this.optionen.nativeElement, 'display', 'block');
    this.isOffen = !this.isOffen;
  }

  public updateAusgewaehlt(i: number, option: HTMLDivElement) {
    this.ausgewaehlt = this.options[i];
    if (this.letzteAktiveOption === null) {
      this.renderer.addClass(option, 'aktiv');
      this.letzteAktiveOption = option;
    } else {
      this.renderer.removeClass(this.letzteAktiveOption, 'aktiv');
      this.renderer.addClass(option, 'aktiv');
      this.letzteAktiveOption = option;
    }
    this.value = this.options[i];
    this.open();
    this.onChange(this.value);
    this.onUpdate.emit();
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  public writeValue(value: any): void {
    this.value = value;
  }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement: any) {
    if (!this.container.nativeElement.contains(targetElement)) {
      this.renderer.setStyle(this.optionen.nativeElement, 'display', 'none');
      this.isOffen = false;
    }
  }

}
