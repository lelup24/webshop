import {Component, Input, OnInit} from '@angular/core';
import {ProduktUebersichtItemModel} from "../../models/produkt-uebersicht-item.model";

@Component({
  selector: 'app-produkt-uebersicht-item',
  templateUrl: './produkt-uebersicht-item.component.html',
  styleUrls: ['./produkt-uebersicht-item.component.scss']
})
export class ProduktUebersichtItemComponent implements OnInit {

  @Input() produkt: ProduktUebersichtItemModel;
  constructor() { }

  ngOnInit(): void {
  }

}
