import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProduktUebersichtItemComponent } from './produkt-uebersicht-item.component';

describe('ProduktUebersichtItemComponent', () => {
  let component: ProduktUebersichtItemComponent;
  let fixture: ComponentFixture<ProduktUebersichtItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProduktUebersichtItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProduktUebersichtItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
