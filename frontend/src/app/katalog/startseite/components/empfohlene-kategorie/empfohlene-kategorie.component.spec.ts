import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpfohleneKategorieComponent } from './empfohlene-kategorie.component';

describe('EmpfohleneKategorieComponent', () => {
  let component: EmpfohleneKategorieComponent;
  let fixture: ComponentFixture<EmpfohleneKategorieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpfohleneKategorieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpfohleneKategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
