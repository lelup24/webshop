import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpfohleneProdukteComponent } from './empfohlene-produkte.component';

describe('EmpfohleneProdukteComponent', () => {
  let component: EmpfohleneProdukteComponent;
  let fixture: ComponentFixture<EmpfohleneProdukteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpfohleneProdukteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpfohleneProdukteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
