import { Component, OnInit } from '@angular/core';
import {ProduktUebersichtItemModel} from "../../../../global/models/produkt-uebersicht-item.model";

@Component({
  selector: 'shop-empfohlene-produkte',
  templateUrl: './empfohlene-produkte.component.html',
  styleUrls: ['./empfohlene-produkte.component.scss']
})
export class EmpfohleneProdukteComponent implements OnInit {

  public produkte: ProduktUebersichtItemModel[] = [{}, {}];

  constructor() { }

  ngOnInit(): void {
  }

}
