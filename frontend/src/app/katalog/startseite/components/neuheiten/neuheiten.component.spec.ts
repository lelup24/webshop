import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeuheitenComponent } from './neuheiten.component';

describe('NeuheitenComponent', () => {
  let component: NeuheitenComponent;
  let fixture: ComponentFixture<NeuheitenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeuheitenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeuheitenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
