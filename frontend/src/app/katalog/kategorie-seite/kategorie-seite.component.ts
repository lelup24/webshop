import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {KategorieService} from "./kategorie.service";


@Component({
  selector: 'shop-kategorie-seite',
  templateUrl: './kategorie-seite.component.html',
  styleUrls: ['./kategorie-seite.component.scss']
})
export class KategorieSeiteComponent implements OnInit, OnChanges {

  constructor(private kategorieService: KategorieService) { }

  ngOnInit(): void {
      this.kategorieService.$ergebnis.subscribe((res:any) => console.log(res.res));
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
