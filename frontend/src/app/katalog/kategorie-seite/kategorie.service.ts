import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class KategorieService {
  $ergebnis: BehaviorSubject<any> = new BehaviorSubject<any>("Hallo");

  constructor(private http: HttpClient) {

  }

  public getKategorieWithSuchwort(offset: string, limit: string, sortOrder: string, kategorie: string, suchwort: string) {
    return this.http.get<any>('/api/produkte/kategorie', {params: {offset, limit, sortOrder, kategorie, suchwort}})
      .subscribe(res => this.$ergebnis.next(res));
  }



}
