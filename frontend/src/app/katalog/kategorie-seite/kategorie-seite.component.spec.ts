import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KategorieSeiteComponent } from './kategorie-seite.component';

describe('KategorieSeiteComponent', () => {
  let component: KategorieSeiteComponent;
  let fixture: ComponentFixture<KategorieSeiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KategorieSeiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KategorieSeiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
